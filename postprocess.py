import os
import pandas as pd
import json
import math
import matplotlib.pyplot as plt

from rocketship import RocketShip

class PostProcess(object):

    def __init__(self, uid, path=os.getcwd()):
        self.uid = uid
        self.path = path
        # endogenous
        self.load_params()
        self.load_posvel()
        self.load_deltav()
        self.load_fuelcost()
        # exogenous
        self.load_launch()
        self.load_ship_storage()
        self.load_fuel_storage()
        self.load_fuel_spill()

    ### LOAD ###

    def get_path(self, filename):
        return os.path.join(self.path, filename)

    def load_params(self):
        filename = self.get_path('params_{}.json'.format(str(self.uid)))
        with open(filename) as json_data:
            params = json.load(json_data)
        self.T = params['T']
        self.s = params['s']
        self.theta = params['theta']
        self.num_ships = params['num_ships']
        self.ship = RocketShip(mass=params['ship_mass'],
                               capacity=params['ship_capacity'],
                               thrust_scale =params['ship_thrust_scale'])
        self.s_nodes = params['s_nodes']
        self.d_nodes = params['d_nodes']
        self.routes = params['routes']

    def load_posvel(self):
        filename = os.path.join('datasets', 'posvel_{}.csv')
        self.posvel = {}
        for i in self.d_nodes + self.s_nodes:
            self.posvel[i] = pd.read_csv(filename.format(i))
            self.posvel[i].drop('Unnamed: 8', axis=1, inplace=True)
            self.posvel[i].columns = map(str.strip, self.posvel[i].columns)

    def load_deltav(self):
        def parse_header(col):
            (i, j, r) = col[1:-1].split(', ')
            return (str(i), str(j), int(r))
        filename = os.path.join('datasets', 'long_deltav2.csv')
        self.deltav = pd.read_csv(filename, index_col=0)
        self.deltav.columns = list(map(parse_header, self.deltav.columns))

    def load_fuelcost(self):
        def parse_header(col):
            (i, j, r) = col[1:-1].split(', ')
            return (str(i), str(j), int(r))
        filename = self.get_path('fuelcost_{}.csv'.format(str(self.uid)))
        self.fuelcost = pd.read_csv(filename, index_col=0)
        for col in self.fuelcost.columns:
            if col.startswith('Unnamed'):
                self.fuelcost.drop(col, axis=1, inplace=True)
        try:
            self.fuelcost.drop('t', axis=1, inplace=True)
        except:
            None
        self.fuelcost.columns = list(map(parse_header, self.fuelcost.columns))

    def load_launch(self):
        def parse_header(col):
            (i, j, r) = col.split(',')
            return (str(i), str(j), int(r))
        filename = self.get_path('launch_{}.csv'.format(str(self.uid)))
        self.launch = pd.read_csv(filename , index_col=0)
        try:
            self.launch.drop('t', axis=0, inplace=True)
        except:
            None
        for col in self.launch.columns:
            if col.startswith('Unnamed'):
                self.launch.drop(col, axis=1, inplace=True)
        self.launch.columns = list(map(parse_header, self.launch.columns))
        self.launch = self.launch.fillna(0)

    def load_ship_storage(self):
        filename = self.get_path('ship_storage_{}.csv'.format(str(self.uid)))
        self.ship_storage = pd.read_csv(filename, index_col=0)

    def load_fuel_storage(self):
        filename = self.get_path('fuel_storage_{}.csv'.format(str(self.uid)))
        self.fuel_storage = pd.read_csv(filename, index_col=0)

    def load_fuel_spill(self):
        filename = self.get_path('fuel_spill_{}.csv'.format(str(self.uid)))
        self.fuel_spill = pd.read_csv(filename, index_col=0)

    ### PLOT ###

    def plot_cum_launches(self):
        self.launch.sum(axis=1).cumsum(axis=0).plot()

    def plot_cum_z(self):
        sp = sum(self.fuel_spill[col] for col in self.d_nodes)
        scale = pd.Series(map(lambda t: math.exp(-self.theta * t / 365), sp.index))
        (sp * scale).cumsum().plot()

    def plot_cum_z_by_node(self):
        idx = range(0, self.T, self.s)
        scale = pd.Series(map(lambda t: math.exp(-self.theta * t / 365), idx))
        for i in self.d_nodes:
            (self.fuel_spill[i] * scale).cumsum().plot(label=i)

    def plot_launch_hist_by_route(self):
        df = self.get_launches_by_route()
        df.plot(kind='bar')

    ### ANALYSIS ###

    def get_z(self):
        sp = sum(self.fuel_spill[col] for col in self.d_nodes)
        scale = pd.Series(map(lambda t: math.exp(-self.theta * t / 365), sp.index))
        scale.index = sp.index
        return (sp * scale).sum()

    def get_burn_to_sell(self):
        burn = self.get_fuel_burn_over_time()
        sell = self.get_fuel_sell_over_time()
        return sell.sum() / burn.sum()

    def get_dist_between(self, i, j):
        dxyz = self.posvel[i][['X', 'Y', 'Z']] - self.posvel[j][['X', 'Y', 'Z']]
        dxyz2 = dxyz.apply(lambda v: v ** 2)
        return dxyz2.sum(axis=1).apply(lambda v: v ** 0.5)

    def get_launches_by_route(self):
        df = pd.DataFrame()
        for r in self.routes:
            df[r] = sum(self.launch[col]
                        for col in self.launch.columns if col[-1] == r)
        return df.sum(axis=0)

    def get_fuel_burn_over_time(self):
        df = pd.DataFrame(self.launch * self.fuelcost,
                          index=self.launch.index, columns=self.launch.columns)
        return df.sum(axis=1)

    def get_fuel_sell_over_time(self):
        return self.fuel_spill[self.d_nodes].sum(axis=1)

    def get_launches_all(self):
        launches = []
        for t in self.launch.index:
            for col in self.launch.columns:
                if self.launch.at[t, col] > 0:
                    launches.append((t, *col))
        return launches

    def get_launches_by_dist(self, i, j):
        launches = []
        for t in self.launch.index:
            for col in self.launch.columns:
                if self.launch.at[t, col] > 10e-3 and col[:2] == (i, j):
                    launches.append((t, *col))
        return launches

    def get_fuelcost_all(self):
        launches = self.get_launches_all()
        return pd.Series([self.fuelcost.at[t, (i, j, r)]
                          for (t, i, j, r) in launches])
