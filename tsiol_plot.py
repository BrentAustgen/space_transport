import base3 as base
from rocketship import RocketShip
from postprocess3 import PostProcess
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

run = {}
for impulse in [900, 925, 950, 975, 1000]:
    for cap in [3e6, 3.5e6, 4e6, 4.5e6, 5e6]:
        run[impulse, cap] = \
            PostProcess('sens_' + str(impulse) + '_' + str(cap / 1e3),
            path='min_max_prod/sens')

impulse = np.array([900, 925, 950, 975, 1000])
cap = np.array([3e6, 3.5e6, 4e6, 4.5e6, 5e6])

df = pd.DataFrame([[run[i, c].z for i in impulse] for c in cap])

for idx in df.index:
    plt.plot(impulse, df.loc[idx])

plt.xlim(xmin=min(impulse), xmax=max(impulse))
#plt.ylim(ymin=min(df), max=max(df))
plt.xlabel('specific impulse (s)')
plt.show()

for col in df.columns:
    plt.plot(cap / 1000, df[col])

plt.xlim(xmin=min(cap / 1000), xmax=max(cap / 1000))
#plt.ylim(ymin=min(df), max=max(df))
plt.xlabel('ship capacity (mT)')
plt.show()
