import base3 as base
from rocketship import RocketShip
from postprocess3 import PostProcess
import numpy as np

run = {}
for impulse in [900, 925, 950, 975, 1000]:
    for cap in [3e6, 4e6, 5e6]:
        run[impulse, cap] = \
            PostProcess('sens_' + str(impulse) + '_' + str(cap / 1e3),
            path='min_max_prod/sens')

for impulse in [900, 925, 950, 975, 1000]:
    for cap in [3e6, 4e6, 5e6]:
        print(round(run[impulse, cap].n), end=', ')
    print()

for impulse in [900, 925, 950, 975, 1000]:
    for cap in [3e6, 4e6, 5e6]:
        this = run[impulse, cap]
        print(this.s.max().max(), end=', ')
    print()

for impulse in [900, 925, 950, 975, 1000]:
    for cap in [3e6, 4e6, 5e6]:
        this = run[impulse, cap]
        print(this.s.sum().sum() / this.d.sum().sum(), end=', ')
    print()

for impulse in [900, 925, 950, 975, 1000]:
    for cap in [3e6, 4e6, 5e6]:
        this = run[impulse, cap]
        print((this.z_ub - this.z_lb) / this.z_ub, end=', ')
    print()

"""
ship = RocketShip(90000, 3000000, thrust_scale=1.93)

mod, results = base.run('solar_elec' + str(ship.thrust_scale),
         'datasets/revamp_deltav.csv',
         gurobi_opts=opts, gamma=100000, ship=ship, tau=10,
         routes=range(20, 1401, 20))
"""

"""
mod, results = base.run('earthmars', 'datasets/revamp_deltav.csv',
         gurobi_opts=opts, gamma=10000, ship=ship, tau=20,
         routes=range(20, 1401, 20), supply_nodes=['Mars', 'Ceres'])
"""
