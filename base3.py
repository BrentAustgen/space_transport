from model3 import *

### WRAPPER ### ---------------------------------------------------------------

def run(uid, deltav_csv, T=None, tau=None, routes=None, demand_nodes=None,
        supply_nodes=None, ship=None, gamma=None,
        gurobi_opts=None, path=None):

    print('starting run with unique id \'{}\''.format(str(uid)))

    print('setting default parameters')
    if tau is None:
        tau = 10
    if routes is None:
        routes = range(20, 1400, 20)
    if T is None:
        T = 14605 - max(routes)
    if demand_nodes is None:
        demand_nodes = ['Earth', 'Mars']
    if supply_nodes is None:
        supply_nodes = ['Ceres']
    if ship is None:
        ship = RocketShip(mass=90000, capacity=3000000, thrust_scale=1)
    if gamma is None: # cost of ship relative to cost of mining operation size
        gamma = 4000
    if gurobi_opts is None:
        gurobi_opts = {'MIPGap': 0.15, 'MIPFocus': 3, 'TimeLimit': 600, 'BranchDir': 1}
    if path is None:
        path = os.getcwd()

    nodes = demand_nodes + supply_nodes
    edges = [(i, j) for (i, j) in product(demand_nodes, supply_nodes) if not i == j] + \
            [(i, j) for (i, j) in product(supply_nodes, demand_nodes) if not i == j]

    print('beginning model construction')
    mod = ConcreteModel()

    print('loading DeltaV data')
    if not os.path.isfile(deltav_csv):
        print('  file {} not found, aborting'.format(deltav_csv))
    deltav_df = pd.read_csv(deltav_csv, index_col=0)

    print('initializing model parameters')
    mod.tau = tau
    mod.time = Set(initialize=range(0, T, tau), ordered=True)
    mod.routes = Set(initialize=routes)
    mod.nodes = Set(initialize=demand_nodes + supply_nodes)
    mod.s_nodes = Set(within=mod.nodes, initialize=supply_nodes)
    mod.d_nodes = Set(within=mod.nodes, initialize=demand_nodes)
    mod.edges = Set(within=mod.nodes * mod.nodes, initialize=edges)
    mod.ship = ship
    mod.gamma = gamma
    mod.fuelcost_df = pd.DataFrame(index=[t for t in mod.time])
    for col in deltav_df:
        if col[0] == 'C' or col.startswith('Mars,Earth'):
            method = mod.ship.vel_to_fuel_full
        else:
            method = mod.ship.vel_to_fuel_empty
        mod.fuelcost_df[col] = deltav_df[col].apply(method)
    mod.c = Param(mod.time, mod.edges, mod.routes, initialize=get_c)
    mod.p = Param(mod.time, mod.edges, mod.routes, initialize=get_p)
    mod.q = Param(mod.time, mod.edges, mod.routes, initialize=get_q)
    mod.d = Param(mod.time, mod.d_nodes, initialize=get_d)

    print('initializing model variables')
    mod.y = Var(mod.time, mod.nodes, domain=NonNegativeIntegers)
    mod.x = Var(mod.time, mod.edges, mod.routes, domain=NonNegativeIntegers)
    mod.f = Var(mod.time, mod.nodes, domain=NonNegativeReals, initialize=0)
    mod.s = Var(mod.time, mod.s_nodes, domain=NonNegativeReals, initialize=0)
    mod.max_s = Var(domain=NonNegativeReals, initialize=0)
    mod.n = Var(domain=NonNegativeIntegers, initialize=0)

    print('initializing model objective function')
    mod.objective = Objective(rule=obj_min_max_supply, sense=minimize)

    print('initializing model constraints')
    mod.con_ship_flow_balance = \
        Constraint(mod.time, mod.nodes, rule=con_ship_flow_balance)

    mod.con_launch_init = Constraint(mod.edges, mod.routes, rule=con_launch_init)
    mod.con_ship_init = Constraint(mod.nodes, rule=con_ship_init)

    mod.con_fuel_flow_balance = \
        Constraint(mod.time, mod.nodes, rule=con_fuel_flow_balance)

    mod.con_fuel_init_cond = Constraint(mod.nodes, rule=con_fuel_init_cond)

    mod.con_route_infeas = \
        Constraint(mod.time, mod.edges, mod.routes, rule=con_route_infeas)

    mod.con_def_max_s = \
        Constraint(mod.time, mod.s_nodes, rule=con_def_max_s)

    mod.con_ships_come_back = Constraint(rule=con_ships_come_back)

#    mod.con_mars_no_supply = Constraint(mod.time, rule=con_mars_no_supply)

    print('preparing to solve')
    solver = SolverFactory('gurobi')
    for (key, val) in gurobi_opts.items():
        solver.options[key] = val
    results = solver.solve(mod, tee=True, keepfiles=True)
    results.write()

    print('postprocessing')
    params = {
        'z': mod.objective.expr(),
        'z_ub': results['Problem'][0]['Upper bound'],
        'z_lb': results['Problem'][0]['Lower bound'],
        'T': T,
        'tau': tau,
        'gamma': gamma,
        'ship_mass': ship.mass,
        'ship_capacity': ship.capacity,
        'ship_thrust_scale': ship.thrust_scale,
        'num_ships': mod.n.value,
        's_nodes': supply_nodes,
        'd_nodes': demand_nodes,
        'routes': [r for r in routes],
        'gurobi_opts': gurobi_opts
    }
    filename = os.path.join(path, 'params_{}.json'.format(str(uid)))
    with open(filename, 'w') as filehandle:
        json.dump(params, filehandle)

    t_idx = [t for t in mod.time]

    df_x = pd.DataFrame(index=t_idx)
    df_c = pd.DataFrame(index=t_idx)
    df_p = pd.DataFrame(index=t_idx)
    df_q = pd.DataFrame(index=t_idx)
    for r in mod.routes:
        for (i, j) in mod.edges:
            x = pd.Series([mod.x[t, i, j, r].value for t in mod.time],
                          index=t_idx)
            df_x[','.join(map(str, [i, j, r]))] = x
            c = pd.Series([mod.c[t, i, j, r] for t in mod.time],
                          index=t_idx)
            df_c[','.join(map(str, [i, j, r]))] = c
            p = pd.Series([mod.p[t, i, j, r] for t in mod.time],
                          index=t_idx)
            df_p[','.join(map(str, [i, j, r]))] = p
            q = pd.Series([mod.q[t, i, j, r] for t in mod.time],
                          index=t_idx)
            df_q[','.join(map(str, [i, j, r]))] = q
    filename = os.path.join(path, 'x_{}.csv'.format(str(uid)))
    df_x.to_csv(filename)
    filename = os.path.join(path, 'c_{}.csv'.format(str(uid)))
    df_c.to_csv(filename)
    filename = os.path.join(path, 'p_{}.csv'.format(str(uid)))
    df_p.to_csv(filename)
    filename = os.path.join(path, 'q_{}.csv'.format(str(uid)))
    df_q.to_csv(filename)

    df_y = pd.DataFrame(index=t_idx)
    for i in mod.nodes:
        df_y[i] = pd.Series([mod.y[t, i].value for t in mod.time], index=t_idx)
    filename = os.path.join(path, 'y_{}.csv'.format(str(uid)))
    df_y.to_csv(filename)

    df_f = pd.DataFrame(index=t_idx)
    for i in mod.nodes:
        df_f[i] = pd.Series([mod.f[t, i].value for t in mod.time], index=t_idx)
    filename = os.path.join(path, 'f_{}.csv'.format(str(uid)))
    df_f.to_csv(filename)

    df_d = pd.DataFrame(index=t_idx)
    for i in mod.d_nodes:
        df_d[i] = pd.Series([mod.d[t, i] for t in mod.time], index=t_idx)
    filename = os.path.join(path, 'd_{}.csv'.format(str(uid)))
    df_d.to_csv(filename)

    df_s = pd.DataFrame(index=t_idx)
    for i in mod.s_nodes:
        df_s[i] = pd.Series([mod.s[t, i].value for t in mod.time], index=t_idx)
    filename = os.path.join(path, 's_{}.csv'.format(str(uid)))
    df_s.to_csv(filename)

    return mod, results
