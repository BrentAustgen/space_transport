import base3 as base
from postprocess3 import PostProcess
import numpy as np
from rocketship import RocketShip

targets = [0, 1, 2, 3, 4, 5, 6, 7]

run = {}
ship = RocketShip(90000, 3000000, thrust_scale=900/467)
for exp in targets:
    run[exp] = \
        PostProcess('gamma_exp' + str(exp), path='min_max_prod/gamma')

for exp in targets:
    print((run[exp].z_ub - run[exp].z_lb) / run[exp].z_ub)

for exp in targets:
    print(np.round(run[exp].n))

for exp in targets:
    print(run[exp].s.max())

for exp in targets:
    print(run[exp].s.sum().sum() / run[exp].d.sum().sum())
