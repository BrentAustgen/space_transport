import base3 as base
from rocketship import RocketShip
import math

opts = {'MIPGap': 0.12, 'MIPFocus': 1, 'TimeLimit': 600}

"""
ship = RocketShip(90000, 3000000, thrust_scale=900/467)
targets = [0, 1, 2, 3, 4, 5, 6, 7]
targets = [5]
for exp in targets:
    mod, results = \
        base.run('gamma_exp' + str(exp),
                 'datasets/revamp_deltav.csv', gurobi_opts=opts,
                 gamma=10**exp, ship=ship, tau=10, routes=range(20, 1401, 20))
"""

# sensitivitiy in impulse and capacity
for impulse in [900, 925, 950, 975, 1000]:
#    for cap in [3e6, 4e6, 5e6]:
    for cap in [3.5e6, 4.5e6]:
        scale = impulse / 467
        ship = RocketShip(90000, cap, thrust_scale=scale)
        mod, results = \
            base.run('sens_' + str(impulse) + '_' + str(cap / 1e3),
                     'datasets/revamp_deltav.csv',
                     gurobi_opts=opts, gamma=100000, ship=ship, tau=10,
                     routes=range(20, 1401, 20))

"""
ship = RocketShip(90000, 3000000, thrust_scale=1.93)

mod, results = base.run('solar_elec' + str(ship.thrust_scale),
         'datasets/revamp_deltav.csv',
         gurobi_opts=opts, gamma=100000, ship=ship, tau=10,
         routes=range(20, 1401, 20))
"""

"""
mod, results = base.run('earthmars', 'datasets/revamp_deltav.csv',
         gurobi_opts=opts, gamma=10000, ship=ship, tau=20,
         routes=range(20, 1401, 20), supply_nodes=['Mars', 'Ceres'])
"""
