from postprocess3 import PostProcess
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

run = PostProcess('', path='min_max_prod/sens')
sch = run.get_launch_schedule()

v = np.arange(0, 30, 0.1)

e = np.array(list(map(run.ship.vel_to_fuel_full, v)))
f = np.array(list(map(run.ship.vel_to_fuel_empty, v)))

plt.plot(v, e, color='red')
plt.plot(v, f, color='blue')
#plt.plot(v, e + f, color='yellow')

for idx in sch.index:
    plt.plot(sch.at[idx, 'Delta-v'], sch.at[idx, 'Cost'], marker='o', markersize=4, color='black')


plt.xlim(xmin=0, xmax=25.5)
plt.xlim(xmin=sch['Delta-v'].min() - 1, xmax=sch['Delta-v'].max() + 1)
plt.ylim(ymin=0, ymax=run.ship.capacity)
plt.show()
