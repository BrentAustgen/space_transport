# -*- coding: utf-8 -*-

from itertools import product
import pandas
from multiprocessing import Process, Pool
import numpy as np

from pykep import lambert_problem, MU_SUN, DAY2SEC, AU


def load_df(filename):
    df = pandas.read_csv(filename, header=0)
    df.columns = map(str.strip, df.columns)
    df.drop(['Calendar Date (TDB)', 'JDTDB', 'Unnamed: 8'], axis=1, inplace=True)
    return df


def solve_lambert(times, route, launch, arrive):

    df_launch = load_df('./datasets/posvel_' + launch + '.csv')
    df_arrive = load_df('./datasets/posvel_' + arrive + '.csv')

    col0 = pandas.Series(index=times)
    colf = pandas.Series(index=times)

    captimes = [t for t in times if t < max(times) - route]
    for t in captimes:

        # data is in km & km/s, so we multiply by 1000 to make it m & m/s
        pos, vel = ['X', 'Y', 'Z'], ['VX', 'VY', 'VZ']
        r0 = df_launch.loc[t, pos].astype(float) * 1000
        rf = df_arrive.loc[t + route, pos].astype(float) * 1000
        v0 = df_launch.loc[t, vel].astype(float) * 1000
        vf = df_arrive.loc[t + route, vel].astype(float) * 1000
        dt = float(route * DAY2SEC)

        # l0 and lf are Lambert problem solutions given in units of m/s
        sols = lambert_problem(r0, rf, dt, MU_SUN)

        # remove orbital body velocities to attain net velocity differences
        Δv0s = [l0 - v0 for l0 in sols.get_v1()]
        Δvfs = [lf - vf for lf in sols.get_v2()]

        # check to see which solution requires least Δv
        rt_sum_sq0 = [sum(v ** 2 for v in Δv0) ** 0.5 / 1000 for Δv0 in Δv0s]
        rt_sum_sqf = [sum(v ** 2 for v in Δvf) ** 0.5 / 1000 for Δvf in Δvfs]
        total = np.array(rt_sum_sq0) + np.array(rt_sum_sqf)
        idx = np.argmin(total)

        # store the most efficient solution
        col0[t] = rt_sum_sq0[idx]
        colf[t] = rt_sum_sqf[idx]

    return col0, colf


def build_dataframe(args):

    (times, launch, arrive, route) = args
    print('{} to {} in {} days'.format(launch, arrive, route))
    base_name = '{},{},{}'.format(launch, arrive, route)
    #col0_name = base_name + '_0'
    #colf_name = base_name + '_f'
    col0, colf = solve_lambert(times, route, launch, arrive)
    #df_DeltaV[col0_name] = col0
    #df_DeltaV[colf_name] = colf
    return (base_name, col0 + colf)


def make_model_data(times, routes, np=4):

    earth_df = load_df('./datasets/posvel_Earth.csv')
    mars_df = load_df('./datasets/posvel_Mars.csv')
    ceres_df = load_df('./datasets/posvel_Ceres.csv')

    demand = ['Earth', 'Mars']
    supply = ['Ceres']

    edges = [(i, j) for (i, j) in product(demand, supply)]
    edges += [(i, j) for (i, j) in product(supply, demand)]
    edges += [(i, j) for (i, j) in product(demand, demand) if not i == j]

    cases = [(times, launch, arrive, route)
             for (launch, arrive) in edges
             for route in routes]

    with Pool(np) as pool:
        results = pool.map(build_dataframe, cases)

    df_deltav= pandas.DataFrame()
    for (name, col) in results:
        df_deltav[name] = col

    return df_deltav
