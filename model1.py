# -*- coding: utf-8 -*-

### IMPORTS ### ---------------------------------------------------------------

from itertools import product
import json
from math import exp, ceil
import pandas as pd
import os

from pyomo.environ import *
from pyomo.opt import *

import data
from rocketship import RocketShip


### HELPER FUNCTIONS ### ------------------------------------------------------

def get_fuel_cost(mod, t, i, j, r):
    return float(mod.fuelcost_df.at[t, '({}, {}, {})'.format(i, j, r)])

def get_fuel_prod(mod, t, i):
    return float(15000 * mod.s)

def edges_leave(mod, i):
    return [edge for edge in mod.edges if edge[0] == i]

def edges_enter(mod, i):
    return [edge for edge in mod.edges if edge[1] == i]


### OBJECTIVE ### -------------------------------------------------------------

def obj_discounted_fuel(mod):
    return sum(mod.fuel_spill[t, i] * exp(-mod.theta / 365 * t)
               for t in mod.time for i in mod.d_nodes)


### CONSTRAINTS ### -----------------------------------------------------------

def con_ship_flow_balance(mod, t, i):
    if t == 0:
        return Constraint.Skip
    return mod.ship_storage[t, i] == mod.ship_storage[t-mod.s, i] - \
        sum(mod.launch[t, e, r]
            for e in edges_leave(mod, i) for r in mod.routes) + \
        sum(mod.launch[t-r, e, r]
            for e in edges_enter(mod, i) for r in mod.routes
            if t-r >= 0)

def con_launch_init_cond(mod, i, j, r):
    return mod.launch[0, i, j, r] == 0

def con_ship_init_cond(mod, i):
    return mod.ship_storage[0, i] == (mod.num_ships if i == 'Ceres' else 0)

def con_fuel_flow_balance_supply(mod, t, i):
    if t == 0:
        return Constraint.Skip
    return mod.fuel_storage[t, i] == mod.fuel_storage[t-mod.s, i] - \
        sum(mod.launch[t, e, r] * mod.ship.capacity
            for e in edges_leave(mod, i) for r in mod.routes) + \
        mod.fuel_prod[t, i] - \
        mod.fuel_spill[t, i]

def con_fuel_flow_balance_demand(mod, t, i):
    if t == 0:
        return Constraint.Skip
    return mod.fuel_storage[t, i] == mod.fuel_storage[t-mod.s, i] - \
        sum(mod.launch[t, e, r] * mod.fuel_cost[t, e, r]
            for e in edges_leave(mod, i) for r in mod.routes) + \
        sum(mod.launch[t-r, e, r] * (mod.ship.capacity - mod.fuel_cost[t-r, e, r])
            for e in edges_enter(mod, i) for r in mod.routes if t-r >= 0) - \
        mod.fuel_spill[t, i]

def con_fuel_init_cond(mod, i):
    return mod.fuel_storage[0, i] == 0

def con_spill_init_cond(mod, i):
    return mod.fuel_spill[0, i] == 0

def con_fuel_cap_supply(mod, t):
    return mod.fuel_storage[t, 'Ceres'] <= \
        ceil(mod.eta * mod.num_ships * mod.ship.capacity)

def con_route_infeas(mod, t, i, j, r):
    if mod.fuel_cost[t, i, j, r] > mod.ship.capacity:
        return mod.launch[t, i, j, r] == 0
    else:
        return Constraint.Skip
