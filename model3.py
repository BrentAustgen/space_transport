# -*- coding: utf-8 -*-

### CHEAT SHEET ### -----------------------------------------------------------
"""
x[t, i, j, r] - launches
c[t, i, j, r] - trip fuel cost, computed from Δv and ship
p[t, i, j, r] - fuel delivered to node j by trip
q[t, i, j, r] - fuel taken from node i for trip
y[t, i]       - ship storage
f[t, i]       - fuel storage
d[t, i]       - demand
s[t, i]       - supply
"""

### IMPORTS ### ---------------------------------------------------------------

from itertools import product
import json
from math import exp, ceil
import pandas as pd
import numpy as np
import os

from pyomo.environ import *
from pyomo.opt import *

import data
from rocketship import RocketShip

np.random.seed(0)

### HELPER FUNCTIONS ### ------------------------------------------------------

def get_c(mod, t, i, j, r):
    # c is cost of trip
    return float(mod.fuelcost_df.at[t, '{},{},{}'.format(i, j, r)])

def get_p(mod, t, i, j, r):
    # q is entering fuel
    return float((mod.ship.capacity - mod.c[t, i, j, r]) if j in mod.d_nodes else 0)

def get_q(mod, t, i, j, r):
    # q is exiting fuel
    return float(mod.ship.capacity if j in mod.d_nodes else mod.c[t, i, j, r])

def get_d(mod, t, i):
    if 2500 < t < 11500:
        if i == 'Mars' and t % 730 == 0:
            return 1000000
        elif i == 'Earth' and t % 730 == 180:
            return 1000000
        elif i == 'Mars' and t % 730 == 360:
            return 1000000
        elif i == 'Earth' and t % 730 == 550:
            return 1000000
        else:
            return 0
    else:
        return 0

def edges_leave(mod, i):
    return [edge for edge in mod.edges if edge[0] == i]

def edges_enter(mod, i):
    return [edge for edge in mod.edges if edge[1] == i]


### OBJECTIVE ### -------------------------------------------------------------

def obj_min_max_supply(mod):
    return mod.max_s + mod.gamma * mod.n #+ \
#        sum(mod.f[max(mod.time), i] for i in mod.s_nodes)


### CONSTRAINTS ### -----------------------------------------------------------

def con_ship_flow_balance(mod, t, i):
    if t == 0:
        return Constraint.Skip
    return mod.y[t, i] == mod.y[t-mod.tau, i] + \
        sum(mod.x[t-r, e, r]
            for e in edges_enter(mod, i) for r in mod.routes if t-r >= 0) - \
        sum(mod.x[t, e, r]
            for e in edges_leave(mod, i) for r in mod.routes)

def con_launch_init(mod, i, j, r):
    return mod.x[0, i, j, r] == 0

def con_ship_init(mod, i):
    return mod.y[0, i] == (mod.n if i == 'Ceres' else 0)

def con_fuel_flow_balance(mod, t, i):
    if t == 0:
        return Constraint.Skip
    return mod.f[t, i] == mod.f[t-mod.tau, i] + \
        sum(mod.x[t-r, e, r] * mod.p[t-r, e, r]
            for e in edges_enter(mod, i) for r in mod.routes if t-r >= 0) - \
        sum(mod.x[t, e, r] * mod.q[t, e, r]
            for e in edges_leave(mod, i) for r in mod.routes) - \
        (0 if not i in mod.d_nodes else mod.d[t, i]) + \
        (0 if not i in mod.s_nodes else mod.s[t, i])

def con_fuel_init_cond(mod, i):
    return mod.f[0, i] == 0

def con_route_infeas(mod, t, i, j, r):
    if mod.c[t, i, j, r] > mod.ship.capacity:
        return mod.x[t, i, j, r] == 0
    else:
        return Constraint.Skip

def con_def_max_s(mod, t, i):
    return mod.max_s >= mod.s[t, i]

def con_ships_come_back(mod):
    return mod.y[max(mod.time), 'Ceres'] == mod.n

def con_mars_no_supply(mod, t):
    return mod.s[t, 'Mars'] <= 0
