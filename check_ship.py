from rocketship import RocketShip
import numpy as np
import matplotlib.pyplot as plt

ship = RocketShip(90000, 9000000, 1.2)
print(ship.thrust_scale * ship.impulse)

v = np.arange(0, 40, 0.1)

e = list(map(ship.vel_to_fuel_empty, v))
e = np.array(e)
f = list(map(ship.vel_to_fuel_full, v))
f = np.array(f)

#plt.plot(v, e + f)
plt.plot(v, f)
plt.plot(v, e)
plt.xlim(xmin=0, xmax=27)
plt.ylim(ymin=0, ymax=ship.capacity*1.05)
plt.show()
